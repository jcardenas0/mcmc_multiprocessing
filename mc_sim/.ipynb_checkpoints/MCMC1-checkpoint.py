"""
Author: Jorge H. Cárdenas
University of Antioquia

"""

from mc_sim.imports import *

import math

class MCMC:
    
    output_data = 0
    
    def __init__(self):
        pass
    

        
    def gaussian_sample(self,params,N):

        theta_sampled=[]
        for key, value in params.items():

            selected_samples = np.random.normal(value["nu"], value["sigma"], N)
            theta_sampled.append(selected_samples[0])

        return theta_sampled

    def mirror_gaussian_sample(self,params,N):

        theta_sampled=[]
        intermediate_sample=[]
        
        for key, value in params.items():

            prior_eval=-np.inf
            
            while prior_eval==-np.inf:
                
                #selected_samples = np.random.uniform(low=value["min"], high=value["max"], size=1)

                selected_samples = np.random.normal(value["nu"], value["sigma"], 1*N)
                prior_eval = self.prior(selected_samples,value["min"],value["max"])

            
            intermediate_sample.append(selected_samples)

        
        return np.mean(np.array(intermediate_sample).T,axis=0)
        #return np.array(intermediate_sample).T

#Assign values based on current state
#Get an array with the evaluation for an specific parameter value in the whole range of X
    
   

    def prior(self,theta,minm, maxm):
        #in this case priors are just the required check of parameter conditions
        #it is unknown.
        #it must return an array with prior evaluation of every theta
        #i evaluate a SINGLE THETA, A SINGLE PARAMETER EVERY TIME
        #depending on which conditional probability i am evaluating

        #m, b, log_f = theta
        if minm < theta < maxm:
            return True
        return -np.inf

    #just assuming everything is in range

    # Metropolis-Hastings

    def set_dataframe(self,parameters):
        columns=['iteration','walker','accepted','likelihood']

        global_simulation_data = pd.DataFrame(columns=columns)  
        new_dtypes = {"iteration": int,"likelihood":np.float64,"walker":int,"accepted":"bool"}
        global_simulation_data[columns] = global_simulation_data[columns].astype(new_dtypes)


        for parameter in parameters:
            global_simulation_data.insert(len(global_simulation_data.columns), parameter,'' )
            new_dtypes = {parameter:np.float64}
            global_simulation_data[parameter] = global_simulation_data[parameter].astype(np.float64)

        return global_simulation_data

    def acceptance(self,new_loglik,old_log_lik):
        if (new_loglik > old_log_lik):

            return True
        else:
            u = np.random.uniform(0.0,1.0)
            # Since we did a log likelihood, we need to exponentiate in order to compare to the random number
            # less likely x_new are less likely to be accepted
            return (u < (np.exp(new_loglik - old_log_lik)))

    def thining(self,dataframe,num_samples ):
        stack = pd.DataFrame()
        walkers = dataframe.walker.unique()
        
        for walker in walkers:
            
            selected = dataframe.loc[dataframe['walker'].isin([walker])]
            
            
            walker_DF = selected.tail(selected.shape[0] - int(num_samples*0.03)) #Dropping
            
            #walker_DF = walker_DF[walker_DF.index % 25 == 0] 
            stack = pd.concat([stack,walker_DF],ignore_index=True)

        
        stack = shuffle(stack)
        #stack = stack.nsmallest(int(num_samples*len(walkers)*0.75),['likelihood']) #Thining

        #walker_DF = walker_DF.sample(n=int(num_samples*0.75), random_state=1)
        
        return stack.sort_values(by=['iteration'])
    
    def MH(self,sky_model,parameters,t_sky_data,sigma_parameter,evaluateLogLikelihood,initial_point,num_walkers,rank,comm,size_cores,num_samples,burn_sample):
        accepted  = 0.0
        row=0
        iteration=0
        thetas_samples=[]


        initial_point = self.gaussian_sample(parameters,1)
        thetas_samples.append(initial_point)
        
        num_of_params=len(initial_point)
        walkers_result=[]
        dataframe = self.set_dataframe(parameters)
        walker=1
        roll = 1
        #this division of rank 0 and others can be helpful to
        #stablish a main node to manage the others

  
        num_sample_min = (int(num_samples/size_cores) * rank )
        num_sample_max = num_sample_min + int(num_samples/size_cores) -1



        if rank==0:
            
            with tqdm(total=int(num_samples/size_cores)) as pbar:

                for n in range(int(num_samples/size_cores)):
                #workers
                    for i in range(1, size_cores):
                        iteration = iteration + 1


                        pbar.update(1)        

                        old_theta = np.array(thetas_samples[len(thetas_samples)-1], copy=True) 

                        #comm.Recv(data_req, source=i, tag=11)
                        req = comm.isend(old_theta, dest=i, tag=11)
                        req.wait()

                        req2 = comm.irecv(source=i, tag=12)
                        
                        old_theta,old_log_lik,new_theta,new_loglik = req2.wait()


                        if self.acceptance(new_loglik,old_log_lik): 
                            accepted = accepted + 1.0  # monitor acceptance
                            
                            data = np.concatenate(([iteration, walker,1, new_loglik],new_theta),axis=0)
                            dataframe.loc[iteration] = data

                        else:

                            data = np.concatenate(([iteration, walker, 0, old_log_lik],old_theta),axis=0)

                            dataframe.loc[iteration] = data
                                        
                    
                        comm.bcast(1, root=0)
                        
                
                comm.bcast(0, root=0)
                return  dataframe



        else:
            while roll==1:
                
                req = comm.irecv(source=0, tag=11)
                    
                old_theta= req.wait()

                old_log_lik = evaluateLogLikelihood(old_theta,t_sky_data.Freq,t_sky_data.t_sky,sigma_parameter)

                params = sky_model.update_parameters(old_theta) #this has impact when using gaussian proposed distribution

                new_theta = self.mirror_gaussian_sample(params,1)                        
                
                new_loglik = evaluateLogLikelihood(new_theta,t_sky_data.Freq,t_sky_data.t_sky,sigma_parameter)

                req = comm.isend([old_theta,old_log_lik,new_theta,new_loglik], dest=0, tag=12)

                req.wait()

                comm.bcast(roll, root=0)
            
            return  np.inf


                    

                    


        
    

